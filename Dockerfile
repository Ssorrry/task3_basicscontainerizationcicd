FROM python:3.9-slim

WORKDIR /app

COPY . .

CMD ["python", "Task2_NumberSearch/Task2_NumberSearch.py", "$SEARCH_VALUE"] 